/*----------------------------------------
	 FOR SVG SPRITE
	 ----------------------------------------*/

svg4everybody();

// var click = document.querySelector('.click-me');

// click.addEventListener('click', function () {
//   alert('ok');
// });

/*----------------------------------------
	 SCROLL MENU
	 ----------------------------------------*/

$(window).scroll(function () {
  if ($(this).scrollTop() > 1) {
    $("header").addClass("glide");
  } else {
    $("header").removeClass("glide");
  }
});

/*----------------------------------------
	 MOBILE MENU
	 ----------------------------------------*/

$(".btn-menu").on("click", function () {
  $(".btn-menu").toggleClass("close-menu");
  $(".menu").toggleClass("menu_open");
  $("body").toggleClass("menu_open");
});

/*----------------------------------------
	 SLICK SLIDER
	 ----------------------------------------*/

// $('.advantages-slider').slick({
//   infinite: true,
// 	dots: true,
// 	slidesToShow: 1,
//   slidesToScroll: 1,
//   variableWidth: true
// });


$(window).on('load resize', function () {
	var width = $(document).width()
	if (width >= 980) {
		if ($('.advantages-slider').hasClass('slick-initialized')) {
			$('.advantages-slider').slick('unslick')
		}
	}
	else {
		$('.advantages-slider').not('.slick-initialized').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true
		})
	}
})

/*----------------------------------------
	 FANCYBOX
	 ----------------------------------------*/

	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
